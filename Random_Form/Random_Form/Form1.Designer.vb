﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRnd
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblAns = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.nudGuess = New System.Windows.Forms.NumericUpDown()
        Me.btnGuess = New System.Windows.Forms.Button()
        CType(Me.nudGuess, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblAns
        '
        Me.lblAns.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblAns.Location = New System.Drawing.Point(52, 106)
        Me.lblAns.Name = "lblAns"
        Me.lblAns.Size = New System.Drawing.Size(170, 33)
        Me.lblAns.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(49, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(173, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Guess a number between 1 and 10"
        '
        'nudGuess
        '
        Me.nudGuess.Location = New System.Drawing.Point(110, 44)
        Me.nudGuess.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nudGuess.Name = "nudGuess"
        Me.nudGuess.Size = New System.Drawing.Size(36, 20)
        Me.nudGuess.TabIndex = 2
        '
        'btnGuess
        '
        Me.btnGuess.Location = New System.Drawing.Point(76, 70)
        Me.btnGuess.Name = "btnGuess"
        Me.btnGuess.Size = New System.Drawing.Size(115, 23)
        Me.btnGuess.TabIndex = 4
        Me.btnGuess.Text = "Guess"
        Me.btnGuess.UseVisualStyleBackColor = True
        '
        'frmRnd
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.btnGuess)
        Me.Controls.Add(Me.nudGuess)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblAns)
        Me.Name = "frmRnd"
        Me.Text = "Random Form"
        CType(Me.nudGuess, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblAns As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents nudGuess As NumericUpDown
    Friend WithEvents btnGuess As Button
End Class
