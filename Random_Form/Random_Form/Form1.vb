﻿Public Class frmRnd
    Private Sub frmRnd_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Randomize()
    End Sub
    Private Sub btnGuess_Click(sender As Object, e As EventArgs) Handles btnGuess.Click
        Dim intAnswer As Integer = CInt(Int((10 * Rnd()) + 1))
        Dim strGuess As Integer = nudGuess.Value
        If intAnswer = strGuess Then
            lblAns.Text = "You got it right!, my number was " + intAnswer.ToString
        ElseIf intAnswer + 1 = strGuess Or intAnswer - 1 = strGuess Then
            lblAns.Text = "You were one off, my number was " + intAnswer.ToString
        Else
            lblAns.Text = "Sorry, but you were too far off, my number was " + intAnswer.ToString
        End If
    End Sub
End Class